# NOTE: I am currently remaking this project in Rust! Old C++ will be kept in a branch for reference, but this is where development will be from now on!

![License](https://badgen.net/gitlab/license/mrmusic25/playlistConverter)
![License](https://badgen.net/gitlab/branches/mrmusic25/playlistConverter?color=orange)
![License](https://badgen.net/gitlab/last-commit/mrmusic25/playlistConverter/rust)
![License](https://badgen.net/gitlab/tags/mrmusic25/playlistConverter?color=purple)

# playlistConverter

A rehash of my [m3uToUSB.sh](https://gitlab.com/mrmusic25/bash-projects) for Bash, written in C++ using ~~Qt~~ and Boost libraries for cross-system compatibility

See [Wiki](https://gitlab.com/mrmusic25/playlistconverter/-/wikis/home) for more info/explanations!

## Installation

1. Install prerequiste programs

   - cmake - needed for installation of taglib (and, eventually, this program as well)

   - libboost-all-dev - needed for file and folder management

   - make - required for installation from source for Boost and TagLib

   - ffmpeg - needed to convert songs to proper format

2. Clone into the repository and enter directory

   `git clone https://gitlab.com/mrmusic25/playlistConverter.git`

   `cd playlistConverter`

#### Automatic installation

From here, run the install script, which will initialize everything and install it to the default path!

`./INSTALL.sh`

OR

#### Manual installation

1. Initialize download and initialize Boost and TagLib submodules (skip this step if installed through package manager!)

   `git submodule update --init --recursive`

   NOTE: Instructions from this point were written for Linux systems

2. Install TagLib from source

   `cd taglib/`

   `cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release .`

   `make`

   `sudo make install`

3. Build the current files using GCC (placeholder until testing phase comes)

   `g++ -std=c++17 playlistConverter.cpp -o playlistConverter $(taglib-config --libs --cflags) -ltag_c -L boost/ -lboost_chrono -lboost_timer -lboost_date_time -lboost_system -lboost_filesystem -fpermissive -lz`

4. (Optional) Link the program to your runpath so it can be used anywhere

   `ln -s "$(pwd)/playlistConverter" "/usr/bin/playlistConverter"`

## Usage

`playlistUpdater [options] <folder_of_playlists>`

OR

`playlistUpdater [options] <playlist.m3u>`

Check `playlistUpdater --help` for info and list of options. Or, see the [Wiki](https://gitlab.com/mrmusic25/playlistconverter/-/wikis/home) for more info!

## Other Info

This program was written for how Apple's iTunes stores and organizes it's music. Therefore, all playlists are <b><u>expected</b></u> to have the following format:

## <b><i>iTunes Library Location</i>/Music/<i>Artist</i>/<i>Album</i>/<i>Song.m4a</i></b>

Slash direction does not matter as it will be localized. As long as it is a valid full path like above, it will be converted.

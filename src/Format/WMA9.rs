//! Holds the static information for the WMA9 (Pro) format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn wma9_format() -> Format {
    Format::new("WMA9".to_string(), 256000, 48000, 16, true, 
    false, "wmapro".to_string(), to_vec(&["wma","asf"]), None)
}

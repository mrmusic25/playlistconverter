//! Holds the static information for the WMA format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn wma_format() -> Format {
    Format::new("WMA".to_string(), 256000, 48000, 16, false, 
    false, "wmav2".to_string(), to_vec(&["wma","asf"]), None)
}

//! Holds the static information for the WMA Lossless format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn wmal_format() -> Format {
    Format::new("WMAL".to_string(), 0, 96000, 24, true, 
    true, "wmalossless".to_string(), to_vec(&["wma","asf"]), None)
}

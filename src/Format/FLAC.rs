//! Holds the static information for the FLAC format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn flac_format() -> Format {
    Format::new("FLAC".to_string(), 0, 655350, 32, true, 
    true, "flac".to_string(), to_vec(&["flac", "oga", "mka", "caf"]), None)
}

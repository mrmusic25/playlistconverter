//! Holds the static information for the AAC format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn aac_format() -> Format {
    Format::new("AAC".to_string(), 256000, 192000, 16, true, 
    false, "aac".to_string(), to_vec(&["m4a", "m4b", "m4r", "m4p", "m4v", "mp4", "3gp"]), Some("m4a".to_string()))
}

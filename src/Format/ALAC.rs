//! Holds the static information for the ALAC format
use super::Format;
use crate::Format::to_vec;

// https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats#Technical_details
pub fn alac_format() -> Format {
    Format::new("ALAC".to_string(), 0, 384000, 32, true,
    true, "alac".to_string(), to_vec(&["m4a","caf","alac"]), Some("m4a".to_string()))
}

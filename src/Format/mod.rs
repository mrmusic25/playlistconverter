//! Definition of supported audio formats
#![allow(non_snake_case)]
use std::cmp::min;

/// Data structure used to define a music file format
#[derive(Clone)]
pub struct Format {
    /// Name of the audio format (not always the same as the file container)
    Name: String,

    /// Maximum bitrate in bits per second
    MaxBitrate: u32,
    
    /// Maximum supported sample rate
    MaxSampleRate: u32,

    /// Maximum supported bit depth
    MaxBitDepth: u32,

    /// Whether the format supports variable bitrate encoding
    SupportsVBR: bool,

    /// Whether the format is lossless
    IsLossless: bool,

    /// The codec to use in FFMpeg for converting to this format
    FFmpegCodec: String,

    /// Valid containers for this format (file extensions)
    ValidContainers: Vec<String>,

    /// The preferred container for this format. If unset, uses the first item in ValidContainers
    PreferredContainer: String
}

/// Functions are common to all formats
impl Format {
    /// Returns a new Format struct
    pub fn new(name: String, max_bitrate: u32, max_sample_rate: u32, max_bit_depth: u32, supports_vbr: bool,
        is_lossless: bool, ffmpeg_codec: String, valid_containers: Vec<String>, preferred_container: Option<String>) -> Self {
        Self {
            Name: name.to_string(),
            MaxBitrate: max_bitrate,
            MaxSampleRate: max_sample_rate,
            MaxBitDepth: max_bit_depth,
            SupportsVBR: supports_vbr,
            IsLossless: is_lossless,
            FFmpegCodec: ffmpeg_codec.to_string(),
            ValidContainers: valid_containers,
            PreferredContainer: preferred_container.unwrap_or("".to_string())
        }
    }

    /// Returns the name of the format.
    pub fn name(&self) -> &String { & self.Name }

    /// Returns the maximum bitrate supported by the format in bits per second.
    pub fn max_bitrate(&self) -> u32 { self.MaxBitrate }

    /// Returns the maximum sample rate supported by the format in Hertz.
    pub fn max_sample_rate(&self) -> u32 { self.MaxSampleRate }

    /// Returns the maximum bit depth supported by the format.
    pub fn max_bit_depth(&self) -> u32  { self.MaxBitDepth }

    /// Returns true if the format supports variable bitrate encoding.
    pub fn supports_vbr(&self) -> bool { self.SupportsVBR }

    /// Returns true if the format is lossless.
    pub fn is_lossless(&self) -> bool { self.IsLossless }

    /// Returns the codec name used in FFmpeg for this format.
    pub fn ffmpeg_codec(&self) -> &String { &self.FFmpegCodec }

    /// Returns a vector of valid container extensions for the format.
    pub fn valid_containers(&self) -> &Vec<String> { &self.ValidContainers }

    /// Returns the maximum bitrate in megabits per second
    pub fn max_mbps(&self) -> u32 {
        self.max_bitrate() / 1000
    }
    
    /// Returns the preferred container for the format
    pub fn preferred_container(&self) -> String {
        if !self.PreferredContainer.is_empty() {
            self.PreferredContainer.to_string()
        } else {
            return if self.ValidContainers.len() > 0 { self.ValidContainers[0].to_string() } else  { "".to_string() }
        }
    }
    
    /// Returns the extension for the format
    pub fn extension(&self) -> String {
        self.preferred_container()
    }

    /// Returns the bitrate to use when converting to this format, taking into account the maximum bitrate
    pub fn get_convert_bitrate(&self, bitrate: u32) -> u32 {
        // We use the minimum here for two reasons to preserve quality:
        //   - Do not up-convert to a higher bitrate
        //   - Only down convert iff current bitrate is higher than codec maximum
        min(bitrate, self.max_bitrate())
    }

    /// Returns the convert bitrate in megabits per second
    pub fn get_convert_bitrate_mbps(&self, bitrate: u32) -> u32 {
        self.get_convert_bitrate(bitrate) / 1000
    }

    /// Returns the sample rate to use when converting to this format
    pub fn get_convert_sample_rate(&self, sample_rate: u32) -> u32 {
        // Using min for the same reasons as get_convert_bitrate
        min(sample_rate, self.max_sample_rate())
    }

    /// Returns whether the format is valid for the given extension
    pub fn valid_format(&self, extension: &String) -> bool {
        self.valid_containers().contains(extension)
    }
}

/// Helper function to convert a variable list of strings to a vector
pub fn to_vec(strings: &[&str]) -> Vec<String> {
    strings.iter().map(|s| s.to_string()).collect()
}

// Now, we need a vector containing all the underlying formats
use once_cell::sync::Lazy;

// Lossy codecs supported
pub mod MP3;
pub mod AAC;
pub mod WMA;
pub mod WMA9;

// Lossless codecs
pub mod FLAC;
pub mod ALAC;
pub mod WMAL;

pub static SUPPORTED_FORMATS : Lazy<Vec<Format>> = Lazy::new(|| {
        vec![
            crate::Format::MP3::mp3_format(),
            crate::Format::AAC::aac_format(),
            crate::Format::WMA::wma_format(),
            crate::Format::WMA9::wma9_format(),
            crate::Format::FLAC::flac_format(),
            crate::Format::ALAC::alac_format(),
            crate::Format::WMAL::wmal_format()
        ]
    }
);

/// Returns the given Format, if it exists
pub fn get_format_from_name(name: &String) -> Option<&Format> { 
    SUPPORTED_FORMATS.iter().find(|f| f.name() == &name.to_uppercase())
}

//! Holds the static information for the MP3 format
use super::Format;

pub fn mp3_format() -> Format {
    Format::new("MP3".to_string(), 320000, 44100, 16, true, 
    false, "libmp3lame".to_string(), vec!["mp3".to_string()], None)
}

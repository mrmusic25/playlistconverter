//! Class file for Song object

use std::path::PathBuf;
use crate::Config::Config;

/// List of actions that can be performed on a song
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum SongAction {
    /// This song will not be touched, only logging that is was processed
    IGNORE,

    /// Convert the song based on Config options
    CONVERT,

    /// Copy the song to the target directory
    COPY,

    /// Create a hardlink to the song, if possible
    HARDLINK,

    /// Create a symbolic link to the song, if possible
    SYMLINK
}

pub struct Song<'a> {
    // File info about song
    input_path: PathBuf,
    output_paths: Vec<PathBuf>,
    config: &'a Config, // We use a reference so that there is only one config that everyone follows
}

impl<'a> Song<'a> {
    pub fn new(input_path: PathBuf, config: &'a Config) -> Song<'a> {
        Song {
            input_path,
            output_paths: vec![],
            config,
        }
    }

    /// Returns a clone of the input path for this song.
    pub fn get_input_path(&self) -> PathBuf {
        self.input_path.clone()
    }

    /// Returns a clone of the output paths for this song.
    pub fn get_output_paths(&self) -> Vec<PathBuf> {
        self.output_paths.clone()
    }

    /// Returns a clone of the config associated with this song.
    pub fn get_config(&self) -> &'a Config {
        self.config.clone()
    }


    /// Adds the given path as an output location for this song.
    /// This path will be appended to the output_folder defined in Config
    pub fn add_output_path(&mut self, output_path: PathBuf) {
        self.output_paths.push(self.config.get_output_folder().join(output_path));
    }
}

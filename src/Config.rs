// Class file for the Config object, which controls the global config of the program
#![allow(non_snake_case)]

use core::panic;
use std::io::Read;
//use log::{error, warn, info, debug, trace};
use colog;

use std::path::PathBuf;
use crate::Format::{Format,get_format_from_name};
use crate::Song::SongAction;
use toml;

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum LinkMode {
    NONE,
    SYMBOLIC,
    HARD
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum FileOutputStructure {
    DEFAULT,
    ARTIST,
    ALBUM,
    FOLDER,
    PLAYLIST
}

pub struct Config {
    // Options for lossy codecs
    default_format: Format,
    default_bitrate: u32,
    default_sample_rate: u32,
    default_bit_depth: u8,

    // Options for lossless codecs
    default_lossless_format: Format,
    default_lossless_bitrate: u32,
    default_lossless_sample_rate: u32,
    default_lossless_bit_depth: u8,

    // General config options
    use_vbr: bool,
    use_lossless: bool,
    supported_formats: Vec<Format>,
    ffmpeg_location: PathBuf,
    ffprobe_location: PathBuf,
    primary_action: SongAction,    // When the song is first processed, do this action
    secondary_action: SongAction,  // Any subsequent processing, do this action
    copy_drm_songs: bool,
    prefer_lossy: bool, // Default is to convert lossless to lossless. If enabled, program will convert lossless to lossy instead (only when conversion is necessary!)

    // Options for creating output directories
    output_playlist_files: bool,
    playlist_extension: String,
    localize_playlists: bool,
    playlist_file_use_posix: bool,
    link_files: bool,
    link_mode: LinkMode,
    relative_links: bool,
    library_folder_prefix: PathBuf,
    output_folder: PathBuf,
    input_playlists_folder: PathBuf,
    input_playlists: Vec<PathBuf>,
    use_whitelist: bool,
    use_blacklist: bool,
    playlist_whitelist: Vec<String>,
    playlist_blacklist: Vec<String>,

    // Options for using iTunes as a playlist source
    itunes_library_location: PathBuf,
    itunes_mode_enabled: bool,
    itunes_use_whitelist: bool,
    itunes_use_blacklist: bool,
    itunes_whitelist: Vec<String>,
    itunes_blacklist: Vec<String>,
}

impl<'a> Config {
    pub fn new() -> Config {
        colog::init();
        Config {
            default_format: get_format_from_name(&"MP3".to_string()).unwrap().clone(),
            default_bitrate: 256000,
            default_sample_rate: 44100,
            default_bit_depth: 16,
            default_lossless_format: get_format_from_name(&"ALAC".to_string()).unwrap().clone(),
            default_lossless_bitrate: 256000,
            default_lossless_sample_rate: 44100,
            default_lossless_bit_depth: 24,
            use_vbr: false,
            use_lossless: false,
            supported_formats: vec![],
            ffmpeg_location: PathBuf::new(),
            ffprobe_location: PathBuf::new(),
            primary_action: SongAction::CONVERT,
            secondary_action: SongAction::COPY,
            copy_drm_songs: false,
            output_playlist_files: false,
            playlist_extension: "m3u".to_string().into(),
            localize_playlists: false,
            playlist_file_use_posix: false,
            link_files: false,
            link_mode: LinkMode::NONE,
            relative_links: false,
            library_folder_prefix: PathBuf::new(),
            output_folder: PathBuf::new(),
            use_blacklist: false,
            use_whitelist: false,
            playlist_blacklist: vec![],
            playlist_whitelist: vec![],
            input_playlists_folder: PathBuf::new(),
            input_playlists: vec![PathBuf::new()],
            itunes_library_location: PathBuf::new(),
            itunes_mode_enabled: false,
            itunes_use_whitelist: false, // I thought about making iTunes use the general white/blacklist by default,
            itunes_use_blacklist: false, //  but I think adding it now will save refactoring later
            itunes_whitelist: vec![],
            itunes_blacklist: vec![],
            prefer_lossy: false
        }
    }

    // Getters for data
    pub fn get_default_format(&self) -> Format {
        self.default_format.clone()
    }

    pub fn get_default_bitrate(&self) -> u32 {
        self.default_bitrate
    }

    pub fn get_default_sample_rate(&self) -> u32 {
        self.default_sample_rate
    }

    pub fn get_default_bit_depth(&self) -> u32 {
        self.default_bit_depth as u32
    }

    pub fn get_default_lossless_format(&self) -> Format {
        self.default_lossless_format.clone()
    }

    pub fn get_default_lossless_bitrate(&self) -> u32 {
        self.default_lossless_bitrate
    }

    pub fn get_default_lossless_sample_rate(&self) -> u32 {
        self.default_lossless_sample_rate
    }

    pub fn get_default_lossless_bit_depth(&self) -> u32 {
        self.default_lossless_bit_depth as u32
    }

    pub fn get_use_vbr(&self) -> bool {
        self.use_vbr
    }

    pub fn get_use_lossless(&self) -> bool {
        self.use_lossless
    }

    pub fn get_supported_formats(&self) -> Vec<Format> {
        self.supported_formats.clone()
    }

    pub fn get_ffmpeg_location(&self) -> PathBuf {
        self.ffmpeg_location.clone()
    }

    pub fn get_ffprobe_location(&self) -> PathBuf {
        self.ffprobe_location.clone()
    }

    pub fn get_primary_action(&self) -> SongAction {
        self.primary_action
    }

    pub fn get_secondary_action(&self) -> SongAction {
        self.secondary_action
    }

    pub fn get_copy_drm_songs(&self) -> bool {
        self.copy_drm_songs
    }

    pub fn get_output_playlist_files(&self) -> bool {
        self.output_playlist_files
    }

    pub fn get_playlist_extension(&self) -> String {
        self.playlist_extension.clone()
    }

    pub fn get_localize_playlists(&self) -> bool {
        self.localize_playlists
    }

    pub fn get_playlist_file_use_posix(&self) -> bool {
        self.playlist_file_use_posix
    }

    pub fn get_link_files(&self) -> bool {
        self.link_files
    }

    pub fn get_link_mode(&self) -> LinkMode {
        self.link_mode
    }

    pub fn get_relative_links(&self) -> bool {
        self.relative_links
    }

    pub fn get_library_folder_prefix(&self) -> PathBuf {
        self.library_folder_prefix.clone()
    }

    pub fn get_output_folder(&self) -> PathBuf {
        self.output_folder.clone()
    }

    pub fn get_input_playlists_folder(&self) -> PathBuf {
        self.input_playlists_folder.clone()
    }

    pub fn get_input_playlists(&self) -> Vec<PathBuf> {
        self.input_playlists.clone()
    }

    pub fn get_use_blacklist(&self) -> bool {
        self.use_blacklist
    }

    pub fn get_use_whitelist(&self) -> bool {
        self.use_whitelist
    }

    pub fn get_playlist_blacklist(&self) -> Vec<String> {
        self.playlist_blacklist.clone()
    }

    pub fn get_playlist_whitelist(&self) -> Vec<String> {
        self.playlist_whitelist.clone()
    }

    pub fn get_prefer_lossy(&self) -> bool {
        self.prefer_lossy
    }
    
    pub fn set_default_format(&mut self, format: Format) {
        self.default_format = format.into();
    }

    pub fn set_default_format_from_string(&mut self, name: String) {
        let fmt = get_format_from_name(&name);
        if fmt.is_none() {
            panic!("Format with name \"{}\" does not exist", name);
        }
        self.default_format = fmt.unwrap().clone().into();
    }

    pub fn set_default_bitrate(&mut self, bitrate: u32) {
        if bitrate <= 0 {
            panic!("Bitrate must be greater than 0");
        }
        if bitrate > 655350 {
            panic!("Bitrate must be less than 655350");
        }
        self.default_bitrate = bitrate;
    }

    pub fn set_default_sample_rate(&mut self, sample_rate: u32) {
        if sample_rate <= 0 {
            panic!("Sample rate must be greater than 0");
        }
        self.default_sample_rate = sample_rate;
    }

    pub fn set_default_bit_depth(&mut self, bit_depth: u8) {
        if bit_depth <= 0 {
            panic!("Bit depth must be greater than 0");
        }
        if bit_depth > 32 {
            panic!("Bit depth must be less than or equal to 32");
        }
        self.default_bit_depth = bit_depth;
    }

    pub fn set_default_lossless_format(&mut self, format: Format) {
        if !format.is_lossless() {
            panic!("Format must be lossless");
        }
        self.default_lossless_format = format.into();
    }

    pub fn set_default_lossless_bitrate(&mut self, bitrate: u32) {
        if bitrate <= 0 {
            panic!("Bitrate must be greater than 0");
        }
        if bitrate > 655350 {
            panic!("Bitrate must be less than 655350");
        }
        self.default_lossless_bitrate = bitrate;
    }

    pub fn set_default_lossless_sample_rate(&mut self, sample_rate: u32) {
        if sample_rate <= 0 {
            panic!("Sample rate must be greater than 0");
        }
        self.default_lossless_sample_rate = sample_rate;
    }

    pub fn set_default_lossless_bit_depth(&mut self, bit_depth: u8) {
        if bit_depth <= 0 {
            panic!("Bit depth must be greater than 0");
        }
        if bit_depth > 32 {
            panic!("Bit depth must be less than or equal to 32");
        }
        self.default_lossless_bit_depth = bit_depth;
    }

    pub fn set_use_vbr(&mut self, use_vbr: bool) {
        self.use_vbr = use_vbr;
    }

    pub fn set_use_lossless(&mut self, use_lossless: bool) {
        self.use_lossless = use_lossless;
    }

    pub fn set_supported_formats(&mut self, formats: Vec<Format>) {
        if formats.len() <= 0 {
            panic!("At least one format must be supported");
        }
        self.supported_formats = formats;
    }

    pub fn set_ffmpeg_location(&mut self, location: String) {
        if location.len() <= 0 {
            panic!("FFmpeg location must not be empty");
        }
        self.ffmpeg_location = PathBuf::from(location);
    }

    pub fn set_ffprobe_location(&mut self, location: String) {
        if location.len() <= 0 {
            panic!("FFprobe location must not be empty");
        }
        self.ffprobe_location = PathBuf::from(location);
    }

    pub fn set_primary_action(&mut self, action: SongAction) {
        if action == SongAction::IGNORE {
            panic!("Primary action must not be IGNORE");
        }
        self.primary_action = action;
    }

    pub fn set_primary_action_from_string(&mut self, name: String) {
        match name.to_uppercase().as_str() {
            "IGNORE" => panic!("Primary action must not be IGNORE"),
            "CONVERT" => self.primary_action = SongAction::CONVERT,
            "COPY" => self.primary_action = SongAction::COPY,
            "HARDLINK" => self.primary_action = SongAction::HARDLINK,
            "SYMLINK" => self.primary_action = SongAction::SYMLINK,
            _ => panic!("Unknown primary action \"{}\"", name)
        }
    }

    pub fn set_secondary_action(&mut self, action: SongAction) {
        self.secondary_action = action;
    }

    pub fn set_secondary_action_from_string(&mut self, name: String) {
        match name.to_uppercase().as_str() {
            "IGNORE" => self.secondary_action = SongAction::IGNORE,
            "CONVERT" => self.secondary_action = SongAction::CONVERT,
            "COPY" => self.secondary_action = SongAction::COPY,
            "HARDLINK" => self.secondary_action = SongAction::HARDLINK,
            "SYMLINK" => self.secondary_action = SongAction::SYMLINK,
            _ => panic!("Unknown secondary action \"{}\"", name)
        }
    }

    pub fn set_copy_drm_songs(&mut self, copy_drm: bool) {
        self.copy_drm_songs = copy_drm;
    }

    pub fn set_output_playlist_files(&mut self, output: bool) {
        self.output_playlist_files = output;
    }

    pub fn set_playlist_extension(&mut self, extension: String) {
        if extension.len() <= 0 {
            panic!("Playlist extension must not be empty");
        }
        self.playlist_extension = extension.into();
    }

    pub fn set_localize_playlists(&mut self, localize: bool) {
        self.localize_playlists = localize;
    }

    pub fn set_playlist_file_use_posix(&mut self, use_posix: bool) {
        self.playlist_file_use_posix = use_posix;
    }

    pub fn set_link_files(&mut self, link: bool) {
        self.link_files = link;
    }

    pub fn set_link_mode(&mut self, mode: LinkMode) {
        if mode == LinkMode::NONE {
            panic!("Link mode must not be NONE");
        }
        self.link_mode = mode;
    }

    pub fn set_relative_links(&mut self, relative: bool) {
        self.relative_links = relative;
    }

    pub fn set_library_folder_prefix(&mut self, prefix: &PathBuf) {
        self.library_folder_prefix = prefix.clone();
    }

    pub fn set_output_folder(&mut self, folder: &String) {
        if folder.len() <= 0 {
            panic!("Output folder must not be empty");
        }
        self.output_folder = PathBuf::from(folder);
    }

    pub fn set_input_playlists_folder(&mut self, folder: &PathBuf) {
        if ! folder.exists() {
            panic!("Input playlists folder must not be empty");
        }
        self.input_playlists_folder = folder.into();
    }

    pub fn set_input_playlists(&mut self, playlists: Vec<PathBuf>) {
        if playlists.is_empty() {
            panic!("At least one playlist must be specified");
        }
        self.input_playlists = playlists;
    }

    pub fn set_itunes_library_location(&mut self, location: PathBuf) {
        if location.as_os_str().len() <= 0 {
            panic!("iTunes library location must not be empty");
        }
        self.itunes_library_location = location;
    }

    pub fn set_itunes_mode_enabled(&mut self, enabled: bool) {
        self.itunes_mode_enabled = enabled;
    }

    pub fn set_itunes_use_whitelist(&mut self, use_whitelist: bool) {
        self.itunes_use_whitelist = use_whitelist;
    }

    pub fn set_itunes_use_blacklist(&mut self, use_blacklist: bool) {
        self.itunes_use_blacklist = use_blacklist;
    }

    pub fn set_itunes_whitelist(&mut self, whitelist: Vec<String>) {
        if whitelist.len() <= 0 {
            panic!("At least one playlist must be whitelisted");
        }
        self.itunes_whitelist = whitelist;
    }

    pub fn set_itunes_blacklist(&mut self, blacklist: Vec<String>) {
        if blacklist.len() <= 0 {
            panic!("At least one playlist must be blacklisted");
        }
        self.itunes_blacklist = blacklist;
    }

    pub fn set_playlist_whitelist(&mut self, whitelist: Vec<String>) {
        if whitelist.len() <= 0 {
            panic!("At least one playlist must be whitelisted");
        }
        self.playlist_whitelist = whitelist;
    }

    pub fn set_playlist_blacklist(&mut self, blacklist: Vec<String>) {
        if blacklist.len() <= 0 {
            panic!("At least one playlist must be blacklisted");
        }
        self.playlist_blacklist = blacklist;
    }

    pub fn set_prefer_lossy(&mut self, prefer_lossy: bool) {
        self.prefer_lossy = prefer_lossy;
    }

    pub fn add_supported_format(&mut self, format: Format) {
        self.supported_formats.push(format);
    }

    pub fn add_supported_format_from_name(&mut self, name: &String) {
        self.supported_formats.push(get_format_from_name(name).unwrap().clone());
    }

    pub fn load_config_from_file(&mut self, file: String) -> Result<(), Box<dyn std::error::Error>> {
        // Make sure we were given a valid file, that it exists, and that it is a valid TOML file
        let file = std::fs::File::open(file);
        if file.is_err() {
            return Err(Box::<dyn std::error::Error>::from("Config file not found"));
        }

        let mut file = file.unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let config: toml::Value = toml::from_str(&contents)?;
        
        // Most options are stored under the [General] section
        if let Some(general) = config.get("General") {
            if let Some(format) = general.get("DefaultFormat") {
                self.set_default_format(get_format_from_name(&String::from(format.as_str().unwrap())).unwrap().clone().into());
            } else {
                eprintln!("Config option General.DefaultFormat is missing");
            }
            if let Some(bitrate) = general.get("DefaultBitrate") {
                self.set_default_bitrate(bitrate.as_integer().unwrap() as u32);
            } else {
                eprintln!("Config option General.DefaultBitrate is missing");
            }
            if let Some(sample_rate) = general.get("DefaultSampleRate") {
                self.set_default_sample_rate(sample_rate.as_integer().unwrap() as u32);
            } else {
                eprintln!("Config option General.DefaultSampleRate is missing");
            }
            if let Some(bit_depth) = general.get("DefaultBitDepth") {
                self.set_default_bit_depth(bit_depth.as_integer().unwrap() as u8);
            } else {
                eprintln!("Config option General.DefaultBitDepth is missing");
            }
            if let Some(lossless_format) = general.get("DefaultLosslessFormat") {
                self.set_default_lossless_format(get_format_from_name(&String::from(lossless_format.as_str().unwrap())).unwrap().clone().into());
                if ! self.default_lossless_format.is_lossless() {
                    panic!("Default lossless format must be lossless");
                }
            } else {
                eprintln!("Config option General.DefaultLosslessFormat is missing");
            }
            if let Some(lossless_bitrate) = general.get("DefaultLosslessBitrate") {
                self.set_default_lossless_bitrate(lossless_bitrate.as_integer().unwrap() as u32);
            } else {
                eprintln!("Config option General.DefaultLosslessBitrate is missing");
            }
            if let Some(lossless_sample_rate) = general.get("DefaultLosslessSampleRate") {
                self.set_default_lossless_sample_rate(lossless_sample_rate.as_integer().unwrap() as u32);
            } else {
                eprintln!("Config option General.DefaultLosslessSampleRate is missing");
            }
            if let Some(lossless_bit_depth) = general.get("DefaultLosslessBitDepth") {
                self.set_default_lossless_bit_depth(lossless_bit_depth.as_integer().unwrap() as u8);
            } else {
                eprintln!("Config option General.DefaultLosslessBitDepth is missing");
            }
            if let Some(use_vbr) = general.get("UseVBR") {
                self.set_use_vbr(use_vbr.as_bool().unwrap());
            } else {
                eprintln!("Config option General.UseVBR is missing");
            }
            if let Some(use_lossless) = general.get("UseLossless") {
                self.set_use_lossless(use_lossless.as_bool().unwrap());
            } else {
                eprintln!("Config option General.UseLossless is missing");
            }
            if let Some(supported_formats) = general.get("SupportedFormats") {
                // If it is an array/vector, programatically convert all elemnts. Else, assume it is a string
                if supported_formats.is_array() {
                    let mut fmtv: Vec<Format> = Vec::new();
                    for fmt in supported_formats.as_array().unwrap() {
                        fmtv.push(get_format_from_name(&String::from(fmt.as_str().unwrap())).unwrap().clone());
                    }
                    self.set_supported_formats(fmtv);
                } else {
                    self.set_supported_formats(Vec::new());
                    self.add_supported_format(get_format_from_name(&String::from(supported_formats.as_str().unwrap())).unwrap().clone());
                }
            } else {
                eprintln!("Config option General.SupportedFormats is missing");
            }
            if let Some(ffmpeg_location) = general.get("FFmpegLocation") {
                self.set_ffmpeg_location(ffmpeg_location.as_str().unwrap().into());
            } else {
                eprintln!("Config option General.FFmpegLocation is missing");
            }
            if let Some(ffprobe_location) = general.get("FFprobeLocation") {
                self.set_ffprobe_location(ffprobe_location.as_str().unwrap().into());
            } else {
                eprintln!("Config option General.FFprobeLocation is missing");
            }
            if let Some(primary_action) = general.get("PrimaryAction") {
                self.set_primary_action_from_string(String::from(primary_action.as_str().unwrap()));
            } else {
                eprintln!("Config option General.PrimaryAction is missing");
            }
            if let Some(secondary_action) = general.get("SecondaryAction") {
                self.set_secondary_action_from_string(String::from(secondary_action.as_str().unwrap()));
            } else {
                eprintln!("Config option General.SecondaryAction is missing");
            }
            if let Some(copy_drm) = general.get("CopyDRMSongs") {
                self.set_copy_drm_songs(copy_drm.as_bool().unwrap());
            } else {
                eprintln!("Config option General.CopyDRMSongs is missing");
            }
            if let Some(output_playlist_files) = general.get("OutputPlaylistFiles") {
                self.set_output_playlist_files(output_playlist_files.as_bool().unwrap());
            } else {
                eprintln!("Config option General.OutputPlaylistFiles is missing");
            }
            if let Some(playlist_extension) = general.get("PlaylistExtension") {
                self.set_playlist_extension(String::from(playlist_extension.as_str().unwrap()));
            } else {
                eprintln!("Config option General.PlaylistExtension is missing");
            }
            if let Some(localize_playlists) = general.get("LocalizePlaylists") {
                self.set_localize_playlists(localize_playlists.as_bool().unwrap());
            } else {
                eprintln!("Config option General.LocalizePlaylists is missing");
            }
            if let Some(playlist_file_use_posix) = general.get("PlaylistFileUsePOSIX") {
                self.set_playlist_file_use_posix(playlist_file_use_posix.as_bool().unwrap());
            } else {
                eprintln!("Config option General.PlaylistFileUsePOSIX is missing");
            }
            if let Some(link_files) = general.get("LinkFiles") {
                self.set_link_files(link_files.as_bool().unwrap());
            } else {
                eprintln!("Config option General.LinkFiles is missing");
            }
            if let Some(link_mode) = general.get("LinkMode") {
                self.set_link_mode(match link_mode.as_str().unwrap() {
                    "NONE" => LinkMode::NONE,
                    "SYMBOLIC" => LinkMode::SYMBOLIC,
                    "HARD" => LinkMode::HARD,
                    _ => panic!("Invalid link mode"),
                });
            } else {
                eprintln!("Config option General.LinkMode is missing");
            }
            if let Some(relative_links) = general.get("RelativeLinks") {
                self.set_relative_links(relative_links.as_bool().unwrap());
            } else {
                eprintln!("Config option General.RelativeLinks is missing");
            }
        }
        if let Some(whitelist) = config.get("Whitelist") {
            let mut whtlst = Vec::new();
            for playlist in whitelist.as_array().unwrap() {
                whtlst.push(String::from(playlist.as_str().unwrap()));
            }
            self.set_playlist_whitelist(whtlst);
        } else {
            eprintln!("Config header Whitelist is missing");
        }
        if let Some(blacklist) = config.get("Blacklist") {
            let mut blklst = Vec::new();
            for playlist in blacklist.as_array().unwrap() {
                blklst.push(String::from(playlist.as_str().unwrap()));
            }
            self.set_playlist_blacklist(blklst);
        } else {
            eprintln!("Config header Blacklist is missing");
        }
        if let Some(itunes) = config.get("iTunes") {
            if let Some(itunes_library_location) = itunes.get("LibraryLocation") {
                self.set_itunes_library_location(PathBuf::from(itunes_library_location.as_str().unwrap()));
            } else {
                eprintln!("Config option iTunes.LibraryLocation is missing");
            }
            if let Some(itunes_mode_enabled) = itunes.get("ModeEnabled") {
                self.set_itunes_mode_enabled(itunes_mode_enabled.as_bool().unwrap());
            } else {
                eprintln!("Config option iTunes.ModeEnabled is missing");
            }
            if let Some(itunes_use_whitelist) = itunes.get("UseWhitelist") {
                self.set_itunes_use_whitelist(itunes_use_whitelist.as_bool().unwrap());
            } else {
                eprintln!("Config option iTunes.UseWhitelist is missing");
            }
            if let Some(itunes_use_blacklist) = itunes.get("UseBlacklist") {
                self.set_itunes_use_blacklist(itunes_use_blacklist.as_bool().unwrap());
            } else {
                eprintln!("Config option iTunes.UseBlacklist is missing");
            }
            if let Some(itunes_whitelist) = itunes.get("Whitelist") {
                let mut whtlst = Vec::new();
                for playlist in itunes_whitelist.as_array().unwrap() {
                    whtlst.push(String::from(playlist.as_str().unwrap()));
                }
                self.set_itunes_whitelist(whtlst);
            } else {
                eprintln!("Config option iTunes.Whitelist is missing");
            }
            if let Some(itunes_blacklist) = itunes.get("Blacklist") {
                let mut blklst = Vec::new();
                for playlist in itunes_blacklist.as_array().unwrap() {
                    blklst.push(String::from(playlist.as_str().unwrap()));
                }
                self.set_itunes_blacklist(blklst);
            } else {
                eprintln!("Config option iTunes.Blacklist is missing");
            }
        } else {
            eprintln!("Config header iTunes is missing");
        }
        Ok(())
    }

    pub fn do_process_playlist(&mut self, playlist_name: String) -> bool {
        if playlist_name.len() <= 0 {
            panic!("Playlist name must not be empty");
        }
        if self.use_blacklist && self.use_blacklist {
            // Do not convert the case of playlists before comparing since names are case-sensitive
            return self.playlist_whitelist.contains(&playlist_name) && !self.playlist_blacklist.contains(&playlist_name);
        }
        if self.use_whitelist && !self.use_blacklist {
            return self.playlist_whitelist.contains(&playlist_name);
        }
        if self.use_blacklist && !self.use_whitelist {
            return !self.playlist_blacklist.contains(&playlist_name);
        }
        true  // Getting this far means both blacklist and whitelist are disabled; allow all playlists in this case
    }

    pub fn do_process_itunes_playlist(&mut self, playlist_name: String) -> bool {
        if playlist_name.len() <= 0 {
            panic!("Playlist name must not be empty");
        }
        if self.itunes_use_blacklist && self.itunes_use_blacklist {
            // Do not convert the case of playlists before comparing since names are case-sensitive
            return self.itunes_whitelist.contains(&playlist_name) && !self.itunes_blacklist.contains(&playlist_name);
        }
        if self.itunes_use_whitelist && !self.itunes_use_blacklist {
            return self.itunes_whitelist.contains(&playlist_name);
        }
        if self.itunes_use_blacklist && !self.itunes_use_whitelist {
            return !self.itunes_blacklist.contains(&playlist_name);
        }
        true  // Getting this far means both blacklist and whitelist are disabled; allow all playlists in this case
    }

} // impl Config

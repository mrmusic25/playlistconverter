//! Main file for the playlistConverter program

// Define modules
pub mod Format;
pub mod Config;
pub mod Song;

use Format::SUPPORTED_FORMATS;

fn main() {
    for format in SUPPORTED_FORMATS.iter() {
        println!("Found format: {}", format.name());
    }
    let conf: Config::Config = Config::Config::new();
    println!("Default format: {}", conf.get_default_format().name());
}

